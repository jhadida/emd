function r = util_frange( f, minb )
%
% range = emd.util_frange( ifreq, minbins=25 )
%
%   Compute default range of instantaneous frequencies for histogram display:
%       - bounds correspond to 1st and 99th percentiles
%       - number of bins is rounded from the bounds' difference
%
%
% JH

    if nargin < 2, minb=25; end
    b = emd.prctile( f, [1,99] );
    n = max( ceil(b(2)-b(1)), minb );
    r = [b,n];

end