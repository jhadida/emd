function p = prctile( x, varargin )
%
% p = emd.prctile( x, p )
%
%   Same as Matlab, but ignores Inf/Nan.
%
% JH

    m = isfinite(x);
    p = prctile( x(m), varargin{:} );

end