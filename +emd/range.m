function r = range( x, r )
%
% Return a range-vector (e.g. for histograms) formatted as:
%   [min, max, nbins]
%
% r = emd.range( x )
%
%   Automatically derive range vector from input data.
%   min/max values are set to [1,99] percentiles, and
%   number of bins to ceil(sqrt(numel(x))).
%
%
% r = emd.range( x, nbins )
% r = emd.range( x, [min,max] )
%
%   Inputs override the defaults computed as previously.
%
%
% r = emd.range( x, [min,max,nbins] )
%
%   Input range is untouched.
%
%
% See also: emd.range2bins
%
% JH

    if nargin < 2, r=[]; end
    if numel(r)==3, return; end
    
    m = isfinite(x);
    bounds = prctile(x(m),[1,99]);
    nbins = ceil(sqrt(nnz(m)));
    
    switch numel(r)
        case 0
            r = [ bounds, nbins ];
        case 1
            r = [ bounds, r ];
        case 2
            r = [ r, nbins ];
        case 3
            % nothing to do
        otherwise
            r = [ r([1,end]), numel(r) ];
    end

end
