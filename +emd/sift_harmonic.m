function y = sift_harmonic( x, fs, amp, omg, off, tol )
%
% y = emd.sift_harmonic( x, fs, amp, omg, off=[0,pi], tol=0.1 )
%
% Harmonic sift with sine base.
% For each phase-offset, compute a sine wave with amplitude amp and
% frequency omg, and get the corresponding masked IMF on signal x.
% 
% The offsets should be paired with their pi-shifts, e.g. offset 0
% should be paired with pi, and pi/2 with -pi/2. This is NOT enforced
% automatically, to allow experimentation.
% 
% The output y is the average IMF across all offsets.
%
%
% See also: emd.sift
%
% References:
%   Deering & Kaiser 2005, "The Use of a Masking Signal to Improve Empirical Mode Decomposition" 
%   (doi: 10.1109/icassp.2005.1416051)
%
% JH

    if nargin < 6, tol=0.1; end
    if nargin < 5, off=0; end

    n = numel(x);
    u = transpose( 2*pi*omg*(0:n-1)/fs );
    
    p = numel(off);
    y = zeros(n,1);
    for i = 1:p
        
        m = amp * sin( u + off(i) );
        y = y + emd.sift( x+m, tol );
        
    end
    y = y/p;
    
end
