function ana = batch_analyse( time, imf, varargin )
%
% ana = emd.batch_analyse( time, imf, ... )
%
%   Run emd.imf_analyse for each set of IMFs in input cell.
%   All arguments are forwarded to emd.imf_analyse.
%
%
%   Output is a struct-array with fields:
%       amp     instantaneous amplitude matrix
%       phs     instantaneous phase matrix
%       frq     instantaneous frequency matrix
%
%
%                           -----------
%                           **WARNING**
%
%   This method can consume enormous amounts of memory; consider running 
%   individual analyses beforehand, in order to determine an appropriate 
%   batch-size.
%
%                           **WARNING**
%                           -----------
%  
%
% See also: emd.batch_emd, emd.imf_analyse
%
% JH

    assert( iscell(imf), 'Input should be a cell of IMF matrices.' );

    n = numel(imf);
    ana = cell(1,n);
    
    fs = 1/(time(2)-time(1));
    for i = 1:n
        ana{i} = emd.imf_analyse( imf{i}, fs, varargin{:} );
    end
    ana = [ana{:}];
    
end