function sp = batch_spectra( time, ana, varargin )
%
% spectra = emd.batch_spectra( time, ana, ... )
%
%   Compute time-frequency spectra from results of IMF analysis.
%
%   All analysis results in the batch should be ALIGNED TO THE 
%   SAME TIMECOURSE, corresponding to the input time vector.
%
%   A common frequency range is determined prior to computing the
%   spectra, unless it is manually specified.
%
%   Output is a struct-array (one for each analysis) with fields:
%       time
%       freq
%       tf
%
% See also: emd.batch_analyse, emd.spectrum 
%
% JH
    
    % find common range for spectra
    flim = arrayfun( @(x) emd.prctile(x.frq,[1,99]), ana, 'UniformOutput', false );
    flim = vertcat(flim{:});
    fmin = min(flim(:,1));
    fmax = max(flim(:,2));

    frange = [ fmin, fmax, max( 25, ceil(fmax-fmin) ) ];
    
    % compute output
    n = numel(ana);
    sp = cell(1,n);
    
    for i = 1:n
        sp{i} = emd.spectrum( time, ana(i), 'frange', frange, varargin{:} );
    end
    sp = [sp{:}];

end
