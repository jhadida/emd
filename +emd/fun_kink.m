function [x,t] = fun_kink(len,fs)
%
% [x,t] = emd.fun_kink( len, fs )
%
% Difficult function to sift.
%
% JH

    kink = load(emd.path('fun-kink.mat'));
    
    t = transpose(0 : (1/fs) : len);
    x = interp1( len*kink.time, kink.vals, t, 'pchip' );
    
    if nargout == 0
        plot( t, x, 'k-', 'LineWidth', 2 );
    end

end