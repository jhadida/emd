function [imf,noise] = emd_ensemble( x, varargin )
%
% [imf,noise] = emd.emd_ensemble( x, nsample )
%
%   Ensemble EMD, using regular sifting.
%   For each noise sample in the ensemble, a set of masked IMFs is produced.
%   The output IMFs are averaged across all samples.
%   Also outputs the noise samples used as masks.
%
%
% [imf,noise] = emd.emd_ensemble( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   snr         DEFAULT: 5
%               Controls the std of noise in the ensemble.
%
%   tol         DEFAULT: 0.1
%               Refine IMF until the centerline is close enough to 0.
%
%   minsd       DEFAULT: 1e-6
%               Stop sifting if signal std is less than this.
%
%   maximf      DEFAULT: 50
%               Stop sifting past this number of IMFs.
%
%
% See also: emd.emd_complete
%
% References:
%   Wu & Huang 2004, "A study of the characteristics of white noise using the empirical mode 
%   decomposition method" (doi: 10.1098/rspa.2003.1221)
%
%   Wu & Huang 2009, "Ensemble Empirical Mode Decomposition: A Noise-Assisted Data Analysis 
%   Method" (doi: 10.1142/s1793536909000047)
%
% JH

    [nsamp,snr,other] = parse_inputs(varargin{:});
    
    % prepare iterations
    x = x(:);
    n = numel(x);
    sigma = std(x(:)) / snr;
    noise = sigma * randn(n,nsamp);
    
    % run EMD with first noise sample to initialise the IMF array
    imf = emd.emd( x + noise(:,1), other );
    
    % run EMDs with other noise samples
    for i = 2:nsamp
        tmp = emd.emd( x + noise(:,i), other );
        imf = combine( imf, tmp );
    end
    
    % return the average IMFs across all noise samples
    imf = imf / nsamp;

end

function a = combine(a,b)
    na = size(a,2);
    nb = size(b,2);
    if na >= nb
        a(:,1:nb) = a(:,1:nb) + b;
    else
        % this should happen less often
        b(:,1:na) = b(:,1:na) + a;
        a = b;
    end
end

function [nsamp,snr,other] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.KeepUnmatched = true;
    p.FunctionName = 'emd.emd_ensemble';
    
    p.addRequired( 'nsample', @(x) v(x,{'double'},{'scalar','positive','integer'}) );
    p.addParameter( 'snr', 5, @(x) v(x,{'double'},{'scalar','positive'}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    nsamp = r.nsample;
    snr = r.snr;
    other = p.Unmatched;

end
