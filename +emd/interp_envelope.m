function [lo,up] = interp_envelope(x,t)
%
% [lo,up] = emd.interp_envelope( x, t=std(x)/1e5 )
%
% Lower and upper envelope interpolation using PCHIP.
% Works fine even if input has no extremum.
%
% Theshold can be used to ignore extrema with small differences.
%
% See also: emd.local_extrema
%
% JH

    assert( isvector(x), 'This function only accepts single-schannel time-courses as vectors.' );
    if nargin < 2, t=std(x)/1e5; end
    n = numel(x);
    
    % local extrema
    [lmin,lmax] = emd.local_extrema(x,t);
    
    % add first and last points manually for "stable" interpolation
    t = transpose(1:n);
    lo = padded_interp(lmin,x,t);
    up = padded_interp(lmax,x,t);
    
    % show result for debug
    if nargout == 0
        
        plot( x, 'k-', 'LineWidth', 2 ); hold on;
        plot( lo, 'b-', 'LineWidth', 1 );
        plot( up, 'r-', 'LineWidth', 1 ); hold off;
        
    end
    
end

function p = padded_interp(k,x,t)
    % NOTE
    %
    % Because this is tied to the emd.local_extrema implementation, we know that:
    %   - k is not empty
    %   - if k has more than 1 index, then it does not contain the first and last points
    %
    n = numel(x);
    if numel(k) == 1
        p = x(k) * ones(n,1);
    else
        v = x(k([ 1, 1:end, end ]));
        %k = [ floor((1+k(1))/2); k; ceil((n+k(end))/2) ];
        k = [ 1; k; n ];
        p = interp1( k, v, t, 'pchip' );
    end
end
