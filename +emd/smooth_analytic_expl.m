function [a,p,f] = smooth_analytic_expl( x, fs )
% 
% [a,p,f] = emd.smooth_analytic_expl( x, fs )
%
%   Smooth analytic features using the Hilbert transform.
%   This is the "explicit" implementation, in which the instantaneous frequency
%   is obtained directly by deriving the unwrapped phase.
%
%
% See also: emd.diff_filter
% 
% JH

    if nargin < 2, fs=1; end
    
    n = size(x,2);
    
    x = hilbert(x);
    a = abs(x);
    p = angle(x);
    
    f = unwrap( p, [], 1 );
    f = emd.diff_filter( f, fs );
    f = max( f, 0 ) / (2*pi);
    p = cumsum([ zeros(1,n); f(1:end-1,:) ],1) * (2*pi/fs);

end