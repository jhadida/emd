function [amp,pts] = waveform( ana, cycle, npts )
%
% [amp,pts] = waveform( ana, cycle, npts=23 )
% 
%   Interpolate the instantaneous amplitude within good cycles for each IMF.
%   Output is a cell of matrices in which each row corresponds to a good cycle.
%
%
% See also: emd.batch_cycles, emd.imf_analyse, emd.cycle_label, emd.cycle_align
%
% JH

    if nargin < 3, npts=23; end
    
    [n,m] = size(ana.phs);
    assert( numel(cycle)==m, 'Size mismatch between cycles and analysis.' );
    
    amp = cell(1,m);
    for i = 1:m
        [amp{i},pts] = emd.cycle_align( ana.phs(:,i), ana.amp(:,i), npts, cycle(i) );
    end
    
    % show for debug
    if nargout == 0
        
        H = emd.gridaxes(m);
        [r,c] = size(H);
        for i = 1:m
            axes(H{i});
            plot( pts, amp{i} );
            f = mean(nonzeros( ana.frq(:,i) )); xlim([0,2*pi]);
            title(sprintf( 'IMF #%d (f=%.2f Hz)', i, f )); 
            if i > (r-1)*c, xlabel('Phase (rad)'); end
            if mod(i,c)==1, ylabel('Amplitude'); end
        end
        
    end

end