function [e,c] = range2bins( r )
%
% [e,c] = emd.range2bins( r )
%
% Convert input range to edges and bin-centres, for use with histograms.
%
% If the number of bins is N, then:
%   e is a vector of length N+1 with e([1,end]) == r(1:2)
%   c is a vector of length N
%
% See also: emd.range
%
% JH

    assert( numel(r)==3 && r(2)>r(1) && r(3)>=2, 'Bad range.' );
    e = linspace( r(1), r(2), r(3)+1 );
    c = (e(1:end-1) + e(2:end))/2;

end
