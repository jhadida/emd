function [a,p,f] = smooth_analytic_impl( x, fs )
% 
% [a,p,f] = emd.smooth_analytic_impl( x, fs )
%
%   Smooth analytic features using the Hilbert transform.
%   This is the "implicit" implementation, in which the instantaneous frequency
%   is obtained indirectly by deriving the real/imaginary parts of the analytic
%   signal.
% 
%
% See also: emd.diff_filter
%
% JH

    if nargin < 2, fs=1; end
    
    n = size(x,2);
    
    x = hilbert(x);
    a = abs(x);
    t = angle(x);
    
    x = cos(t);
    y = sin(t);
    
    f = x .* emd.diff_filter( y, fs ) - y .* emd.diff_filter( x, fs );
    f = max( f, 0 ) / (2*pi);
    p = cumsum([ zeros(1,n); f(1:end-1,:) ],1) * (2*pi/fs);

end