function p = path(varargin)
%
% p = emd.path(...)
%
% Behave as fullfile, but relative to this toolbox's path.
%
% JH

    p = fileparts(mfilename('fullpath'));
    p = fullfile( p, varargin{:} );

end