function [imf,noise] = emd_complete( x, varargin )
%
% [imf,noise] = emd.emd_complete( x, nsample )
%
%   Complete ensemble EMD, using ensemble sifting.
%   For each noise sample in the ensemble, a set of masked IMFs is produced.
%   The difference with ensemble EMD is that the noise samples are sifted
%   along with the input signal.
%   The output IMFs are averaged across all samples.
%   Also outputs the sifted noise samples used as masks (sort of residual).
%
%
% [imf,noise] = emd.emd_complete( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   snr         DEFAULT: 5
%               Controls the std of noise in the ensemble.
%
%   tol         DEFAULT: 0.1
%               Refine IMF until the centerline is close enough to 0.
%
%   minsd       DEFAULT: 1e-6
%               Stop sifting if signal std is less than this.
%
%   maximf      DEFAULT: 50
%               Stop sifting past this number of IMFs.
%
%
% See also: emd.sift_ensemble, emd.emd_ensemble
%
% References:
%   Torres 2011, "A complete ensemble empirical mode decomposition with adaptive noise" 
%   (doi: 10.1109/icassp.2011.5947265)
%
% JH

    [nsamp,snr,tol,minsd,maximf] = parse_inputs(varargin{:});
    
    % prepare iterations
    x = x(:);
    n = numel(x);
    it = 0;
    imf = cell(1,maximf);
    sigma = std(x(:)) / snr;
    noise = sigma * randn(n,nsamp);
    
    % iterate to extract each IMF
    while (std(x) > minsd) && (it < maximf)
        it = it+1;
        [imf{it},noise] = emd.sift_ensemble(x,noise,tol);
        x = x - imf{it};
    end
    
    % concatenate IMFs in reverse order
    if it == 0
        warning('Could not sift: returning untouched input.');
        imf = x;
    else
        imf = fliplr(horzcat( imf{1:it} ));
    end

end

function [nsamp,snr,tol,minsd,maximf] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.FunctionName = 'emd.emd_complete';
    
    p.addRequired( 'nsample', @(x) v(x,{'double'},{'scalar','positive','integer'}) );
    p.addParameter( 'snr', 5, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'tol', 0.1, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'minsd', 1e-6, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'maximf', 50, @(x) v(x,{'double'},{'scalar','positive','integer'}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    nsamp = r.nsample;
    snr = r.snr;
    tol = r.tol;
    minsd = r.minsd;
    maximf = r.maximf;

end
