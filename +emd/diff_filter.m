function dx = diff_filter( x, fs )
%
% dx = emd.diff_filter( x, fs )
%
% Noise-robust differentiation with FIR filter of order 9.
%
% References:
%   Hadida 2018, "Noise Robust Differentiation", PhD Thesis pp.195-196
%   (url: https://ora.ox.ac.uk/objects/uuid:5abd962a-b798-4530-947a-24eeafd568f3)
%
% JH

    if nargin < 2, fs=1; end
    
    h = [27,16,-1,-2]/96;
    h = [ -fliplr(h), 0, h ] * fs;
    h = flipud(h(:));
    
    dx = wextend( 'ar', 'sp1', x, 4 );
    dx = conv2( dx, h, 'valid' );

end
