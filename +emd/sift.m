function x = sift( x, tol )
%
% x = emd.sift( x, tol=0.1 )
%
% Regular sifting.
%
% See also: emd.interp_envelope
%
% References:
%   Huang 1998, "The empirical mode decomposition and the Hilbert spectrum for nonlinear 
%   and non-stationary time series analysis" (doi: 10.1098/rspa.1998.0193)
%
% JH

    if nargin < 2, tol=0.1; end
    
    %shg; subplot(2,1,1); plot(x,'k','LineWidth',2); title('Original');

    r = inf;
    while r > tol
        [lo,up] = emd.interp_envelope(x);
        m = (lo+up)/2;
        r = sum(m.*m) / max(eps,sum(x.*x));
        x = x - m;
        
        %subplot(2,1,2); plot(x,'k'); title(sprintf('IMF trial (r=%g)',r)); pause(1);
    end
    %subplot(2,1,2); plot(x,'r'); title(sprintf('IMF final (r=%g)',r)); pause(3);
    
end