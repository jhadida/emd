
% ==================================================
% 1. Prepare time-series data
% ==================================================

% struct with fields "time" and "vals" (TxN matrix)
data = emd.data_resting_meg();


% ==================================================
% 2. Options
% ==================================================

% EMD method to use
% see: emd.batch_emd
method              = 'regular';

% options for the EMD method selected
% see: emd.emd, emd.emd_harmonic, emd.emd_ensemble, emd.emd_complete
opt_emd.tol         = 0.1;
opt_emd.maximf      = 50;

% options for analysing IMFs
% see: emd.imf_analyse
opt_imf.method      = 'huang';

% options for computing time-frequency spectra
% see: emd.spectrum
opt_sp.mode         = 'magnitude';
opt_sp.trange       = ceil(numel(data.time)/20);    % divide sampling rate by 20
opt_sp.frange       = [1, 35, 100];                 % 100 freq. bins between 1-35 Hz


% ==================================================
% 3. Run analysis and show spectra
% ==================================================

% see: emd.run
[spectra,analysis,IMF] = emd.run( ...
    data.time, data.vals, method, ...
    opt_emd, opt_imf, opt_sp ...
);

% show time-frequency spectrum averaged across channels
figure; emd.show_spectra( spectra, @(x) mean(x,3) );
