function [x,e] = sift_ensemble( x, e, tol )
%
% [x,e] = emd.sift_ensemble( x, e, tol=0.1 )
%
% Ensemble sifting.
% Each column of e:
%   - is used to compute a masked IMF on input signal x, 
%   - and is then subtracted its own first IMF.
% 
% The output x is the average of all IMFs obtained across noise samples.
% The output e is a matrix with the noise samples with first IMF subtracted.
%
%
% See also: emd.sift
%
% References:
%   Torres 2011, "A complete ensemble empirical mode decomposition with adaptive noise" 
%   (doi: 10.1109/icassp.2011.5947265)
%
% JH

    if nargin < 3, tol=0.1; end
    
    n = size(e,2);
    
    % initialise output with first noise sample
    y = emd.sift( x + e(:,1), tol );
    e(:,1) = e(:,1) - emd.sift( e(:,1), tol );
    
    % iterate over other samples
    for i = 2:n
        y = y + emd.sift( x + e(:,i), tol );
        e(:,i) = e(:,i) - emd.sift( e(:,i), tol );
    end
    
    % final IMF estimate
    x = x - y/n;

end
