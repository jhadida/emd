function H = gridaxes( n )
%
% H = gridaxes( num )
%
%   Create grid of subplots to fit specified number of axes in current figure.
%   Output is a cell of axes handles.
%
%
% JH

    r = 16/9;
    nr = ceil(sqrt( n/r ));
    nc = ceil(n / nr);
    
    H = cell(nr,nc);
    for i = 1:n
        H{i} = subplot(nr,nc,i);
    end

end