function imf = emd( x, varargin )
%
% imf = emd.emd( x )
%
%   Regular EMD, using regular sifting.
%   Returns a matrix of IMFs in columns.
%
%
% imf = emd.emd( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   tol         DEFAULT: 0.1
%               Refine IMF until the centerline is close enough to 0.
%
%   minsd       DEFAULT: 1e-6
%               Stop sifting if signal std is less than this.
%
%   maximf      DEFAULT: 50
%               Stop sifting past this number of IMFs.
%
%
% See also: emd.sift
%
% References:
%   Huang 1998, "The empirical mode decomposition and the Hilbert spectrum for nonlinear 
%   and non-stationary time series analysis" (doi: 10.1098/rspa.1998.0193)
%
% JH

    [tol,minsd,maximf] = parse_inputs(varargin{:});
    
    % prepare iteration
    x = x(:);
    it = 0;
    imf = cell(1,maximf);
    
    % iterate to extract each IMF
    while (std(x) > minsd) && (it < maximf)
        it = it+1;
        imf{it} = emd.sift(x,tol);
        x = x - imf{it};
    end
    
    % concatenate IMFs in reverse order
    if it == 0
        warning('Could not sift: returning untouched input.');
        imf = x;
    else
        imf = fliplr(horzcat( imf{1:it} ));
    end
    
end

function [tol,minsd,maximf] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.FunctionName = 'emd.emd';
    
    p.addParameter( 'tol', 0.1, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'minsd', 1e-6, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'maximf', 50, @(x) v(x,{'double'},{'scalar','positive','integer'}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    tol = r.tol;
    minsd = r.minsd;
    maximf = r.maximf;

end
