function n = nzc(x)
%
% n = emd.nzc(x)
% 
% Quick way to compute number of zero-crossings.
%
% JH

    n = nnz( x(1:end-1) .* x(2:end) <= 0 );

end
