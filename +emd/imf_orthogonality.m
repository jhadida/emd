function orth = imf_orthogonality(imf)
%
% orth = imf_orthogonality(imf)
%
% Returns symmetric Nimf-by-Nimf matrix where element (i,j) is
%   orth(i,j) = sin( imf(:,i), imf(:,j) )
%
% such that value is 1 if IMFs i and j are perfectly orthogonal, 
% or 0 if they are perfectly aligned (though maybe sign-flipped).
%
% JH

    orth = transpose(imf) * imf;
    self = diag(orth);
    orth = orth.^2 ./ (self * transpose(self));
    orth = sqrt(1 - orth);

end