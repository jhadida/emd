function [ana,cycle] = batch_smooth( time, ana, cycle, varargin )
%
% [ana,cycle] = batch_smooth( time, ana, cycle=[], ... )
%
%   Smooth the phase to remove within-cycle variations, and reestimate the frequency.
%   Automatically label cycles (with default options) if third input is omitted.
%   Additional inputs are forwarded to emd.cycle_label.
%
%
% See also: emd.cycle_label, emd.interp_phase
%
% JH
    
    n = numel(ana);
    if nargin < 3 || isempty(cycle)
        cycle = emd.batch_cycles( ana, varargin{:} ); 
    end
    assert( numel(cycle)==n, 'Size mismatch between cycle and analysis.' );
    fs = 1/(time(2)-time(1));
    
    % for each set in the batch
    for i = 1:n
        
        m = size( ana(i).phs, 2 );
        assert( numel(cycle{i})==m, 'Size mismatch at index %d.', i );
        
        % smooth the phase to remove within-cycle variations
        for j = 1:m
            
            % current phase signal and corresponding cycles
            p = ana(i).phs(:,j);
            c = cycle{i}(j);

            % interpolate the phase envelope
            %emd.interp_phase( p, c.index ); pause(1);
            [lo,up] = emd.interp_phase( p, c.index );
            p = (lo+up)/2;
            
            % recompute the cycles
            [c.label, c.index] = emd.cycle_label( p, varargin{:} );
            
            ana(i).phs(:,j) = p;
            cycle{i}(j) = c;
            
        end

        % reestimate the frequencies
        ana(i).frq = emd.diff_filter( ana(i).phs, fs ) / (2*pi);
        
    end

end