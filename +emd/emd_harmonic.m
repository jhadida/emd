function [imf,freq] = emd_harmonic( x, varargin )
%
% [imf,freq] = emd.emd_harmonic( x, fs )
%
%   Harmonic EMD, using harmonic sifting.
%   The successive IMFs are computed by masking the remaining signal with
%   harmonic functions of decreasing frequency (cf reduction factor). 
%   For a given harmonic mask, several IMFs are computed by gradually shifting 
%   the phase of the mask by multiple offsets, and the final IMF for that 
%   frequency is averaged across all offsets.
%   Also returns a vector of frequencies (in Hz) corresponding to the harmonic
%   mask for each IMF.
%
%
% [imf,freq] = emd.emd_harmonic( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   amplitude   DEFAULT: imf
%               Controls the amplitude of harmonic masks:
%                   
%                   imf     amplitude matches the std of successive IMFs
%                   sig     fixed amplitude corresponding to singal std
%               
%               Alternatively, specify a fixed amplitude as a scalar.    
%
%   frequency   DEFAULT: 0
%               The starting frequency, used for the first harmonic mask.
%               If zero, it is estimated using emd.util_bandwidth.
%
%   factor      DEFAULT: 2
%               Reduction factor for the frequency of harmonic masks.
%
%   offset      DEFAULT: (0:3)*(pi/2)
%               Phase-offsets for each harmonic mask.
%               See emd.sift_harmonic for details.
%
%   tol         DEFAULT: 0.1
%               Refine IMF until the centerline is close enough to 0.
%
%   minsd       DEFAULT: 1e-6
%               Stop sifting if signal std is less than this.
%
%   maximf      DEFAULT: 50
%               Stop sifting past this number of IMFs.
%
%
% See also: emd.sift_harmonic, emd.util_bandwidth
%
% References:
%   Deering & Kaiser 2005, "The Use of a Masking Signal to Improve Empirical Mode Decomposition" 
%   (doi: 10.1109/icassp.2005.1416051)
%
% JH

    [fs,amp,freq,fac,off,tol,minsd,maximf] = parse_inputs(varargin{:});
    
    % prepare iterations
    x = x(:);
    it = 0;
    
    % process inputs
    if ischar(amp)
        switch lower(amp)
            case {'s','sig','signal'}
                a = std(x);
                amp = 'signal';
            case {'i','imf'}
                a = std(x); % start with signal std
                amp = 'imf';
            otherwise
                error( 'Unknown amplitude option: %s', amp );
        end
    else
        a = amp;
        amp = 'manual';
    end
    
    if freq < eps
        [~,f] = emd.util_bandwidth( x, fs );
    else
        f = freq;
    end
    freq = zeros(1,maximf);
    
    % iterate to extract each IMF
    imf = cell(1,maximf);
    while (std(x) > minsd) && (it < maximf)
        
        it = it+1;
        freq(it) = f;
        imf{it} = emd.sift_harmonic( x, fs, a, f, off, tol );
        
        %subplot(2,1,1); plot(x,'k-','LineWidth',2); title('Remaining');
        %subplot(2,1,2); plot(imf{it},'r-'); title('IMF'); pause(4);
        
        x = x - imf{it};
        f = f/fac;
        
        if strcmp(amp,'imf')
            a = std(imf{it});
        end
        
    end
    
    % concatenate IMFs in reverse order
    if it == 0
        warning('Could not sift: returning untouched input.');
        imf = x;
    else
        imf = fliplr(horzcat( imf{1:it} ));
    end

end

function [fs,amp,freq,fac,off,tol,minsd,maximf] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.FunctionName = 'emd.emd_harmonic';
    
    p.addRequired( 'fs', @(x) v(x,{'double'},{'scalar','positive'}) );
    
    p.addParameter( 'amplitude', 'imf', @(x) v(x,{'double','char'}) );
    p.addParameter( 'frequency', 0, @(x) v(x,{'double'},{'scalar','nonnegative'}) );
    p.addParameter( 'factor', 2, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'offset', (0:3)*(pi/2), @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'tol', 0.1, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'minsd', 1e-6, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'maximf', 50, @(x) v(x,{'double'},{'scalar','positive','integer'}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    fs = r.fs;
    amp = r.amplitude;
    freq = r.frequency;
    fac = r.factor;
    off = r.offset;
    tol = r.tol;
    minsd = r.minsd;
    maximf = r.maximf;

end
