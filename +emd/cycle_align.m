function [aligned,pts] = cycle_align( phase, value, npts, cycle )
%
% [aligned,pts] = emd.cycle_align( phase, values, npts=23, cycle=[] )
%
%   Interpolate values within each good cycle at a fixed number of points 
%   around the circle.
%
%
% **NOTE** 
%   THIS FUNCTION DOES NOT ACCEPT PHASE MATRICES
%   The columns of the phase matrix output by emd.imf_analyse should be 
%   processed individually.
%
%
% INPUTS
% ------
%
%   npts        DEFAULT: 23
%               Number of points around the circle.
%
%   cycles      DEFAULT: computed using emd.cycle_label
%               Nx2 array of first/last timepoint index for each cycle.
%
%
% OUTPUTS
% -------
%
%   aligned     Ncycles-by-Npts matrix with interpolated values.
%
%   pts         1-by-Npts vector with corresponding interpolants.
%
%
% JH

    n = numel(phase);
    assert( isvector(phase), 'This function does not accept phase matrices.' );
    assert( numel(value)==n, 'Size mismatch between phase and values.' );
    
    phase = phase(:);
    value = value(:);

    % compute cycles if needed
    if nargin < 3 || isempty(npts), npts=23; end
    if nargin < 4
        [~,cycle] = emd.cycle_label( phase );
    elseif isstruct(cycle)
        cycle = cycle.index;
    end
    
    % interpolants
    pts = linspace(0,2*pi,npts+1);
    pts = (pts(1:end-1) + pts(2:end))/2;
    
    % iterate over cycles
    nc = size(cycle,1);
    aligned = zeros( nc, npts );
    for i = 1:nc
        k = cycle(i,:);
        k = k(1):k(2);
        p = mod( phase(k), 2*pi );
        v = value(k);
        aligned(i,:) = interp1( p, v, pts, 'pchip', mean(v) );
    end

end