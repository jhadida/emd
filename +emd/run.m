function [sp,ana,imf] = run( t, x, method, opt_emd, opt_imf, opt_sp )
%
% [spectra,analysis,imf] = emd.run( t, x, method, opt_emd, opt_imf, opt_sp )
%
%   Run EMD analysis on input time-courses.
%
%
% See also: emd.batch_emd, emd.batch_analyse, emd.batch_spectra
%
% JH

    if nargin < 6, opt_sp=struct(); end
    if nargin < 5, opt_imf=struct(); end
    if nargin < 4, opt_emd=struct(); end
    if nargin < 3, method='emd'; end
    
    imf = emd.batch_emd( t, x, method, opt_emd );
    ana = emd.batch_analyse( t, imf, opt_imf );
    sp = emd.batch_spectra( t, ana, opt_sp );
    
end