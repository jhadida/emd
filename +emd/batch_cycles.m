function cyc = batch_cycles( ana, varargin )
%
% cycles = emd.batch_cycles( ana, ... )
%
%   Find "good cycles" for each phase signal corresponding to an IMF in the input
%   analysis structure. This method does not require all phase matrices to be aligned 
%   to the same timecourse, but just make sure you know what you are doing...
%
%   Output is a cell of struct-arrays with fields:
%       label
%       index
%   where each cell corresponds to a different analysis struct (i.e. different IMF set),
%   and each struct corresponds to a different IMF.
%
%
% See also: emd.batch_analyse, emd.cycle_label
%
% JH

    n = numel(ana);
    cyc = cell(1,n);
    tmp = struct( 'label', [], 'index', [] );
    
    for i = 1:n
        m = size(ana(i).phs,2);
        ci = repmat( tmp, 1, m );
        for j = 1:m
            [ci(j).label, ci(j).index] = emd.cycle_label( ana(i).phs(:,j), varargin{:} );
        end
        cyc{i} = ci;
    end

end