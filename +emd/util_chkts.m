function [nt,ns,fs] = util_chkts(t,x)
%
% [nt,ns,fs] = emd.util_chkts(t,x)
%
%   Validate input time vector and data matrix, and extract useful properties.
%
% JH

    assert( isvector(t) && ismatrix(x), 'Bad format: expected t vector, and x matrix.' );
    
    [nt,ns] = size(x);
    assert( numel(t)==nt, 'Size mismatch: data matrix x should be time-by-channels.' );
    
    dt = diff(t);
    fs = 1/(t(2)-t(1));
    
    assert( all(dt > eps) && fs*std(dt) < 1e-6, 'Timepoints are not equally spaced.' );

end