function [x,t] = fun_comsin( len, fs )
%
% [x,t] = emd.fun_comsin( len, fs )
%
% Composite sinusoidal signal with AM modulation and flat borders.
% 
% JH

    if nargin < 2, fs=25; end
    if nargin < 1, len=3; end

    t = transpose(0 : (1/fs) : len);
    x = 2*pi*t;
    x = sin(x) + sin(5*x);
    x = x .* ( t>=t(1)+len/5 & t<=t(end)-len/5 );

    if nargout == 0
        plot( t, x, 'k-', 'LineWidth', 2 );
    end
    
end
