function [lab,fl] = cycle_label( phase, rtol, minlen )
%
% [label,index] = emd.cycle_label( phase, rtol=3, minlen=5 )
%
%   Find and label "good cycles" in the input phase vector.
%   A good cycle is one where:
%       - the phase is strictly increasing
%       - there are enough timepoints (cf minlen)
%       - the first phase value is close to 0 (cf rtol)
%       - the last phase value is close to 2*pi
%
%   The thresholds for phase-distance and minimum number of timepoints are 
%   controlled by inputs options.
%
%
% **NOTE** 
%   THIS FUNCTION DOES NOT ACCEPT PHASE MATRICES
%   The columns of the phase matrix output by emd.imf_analyse should be 
%   processed individually.
% 
%   
% OPTIONS
% -------
%
%   rtol        DEFAULT: 3
%               Threshold on phase distance from 0 and 2*pi.
%               Specified in relative units of median absolute phase-step.
%
%   minlen      DEFAULT: 5
%               Threshold on number of timepoints per cycle.
%
%
% OUTPUTS
% -------
%
%   label       int32 vector of same size as input phase, indicating the cycle ID 
%               for each timepoint (or 0 if not in a good cycle).
%   
%   index       Nx2 matrix with first/last timepoint of good cycles in each row.
% 
%
% JH

    if nargin < 2, rtol=3; end
    if nargin < 3, minlen=5; end
    valid = @validateattributes;
    valid( rtol, {'double'}, {'positive','scalar'} );
    valid( minlen, {'double'}, {'positive','scalar','integer','>',2} );
    
    n = numel(phase);
    assert( isvector(phase), 'This function does not accept phase matrices.' );
    phase = mod( phase(:), 2*pi );
    
    % find timepoints with increasing phase
    good = diff(phase);
    tol = rtol * median(abs(good));
    good = good > 0;
    good = [good; good(end)];
    lab = zeros(n,'int32');
    
    % find large-enough segments
    ind = bwconncomp(good,4);
    ind = ind.PixelIdxList;
    ind = ind(cellfun(@numel, ind) >= minlen);
    
    % check first and last phase of each cycle
    fl = cellfun( @(k) k([1,end]), ind, 'UniformOutput', false );
    fl = transpose(horzcat( fl{:} ));
    if isempty(fl), return; end
    
    ok = reshape( phase(fl), size(fl) );
    ok = mod( ok, 2*pi );
    ok = ok(:,1) < tol & 2*pi-ok(:,2) < tol;

    % assign label to each cycle
    ind = ind(ok);
    m = numel(ind);
    for i = 1:m
        lab(ind{i}) = i;
    end
    
end
