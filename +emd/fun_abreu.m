function [x,t] = fun_abreu( len, fs, omg, deg, phi )
%
% [x,t] = emd.fun_abreu( len, fs, omg, deg, phi )
%
% Generate non-linear waveform using eq 7 in:
%   Abreu 2010, "Analytical approximate wave form for asymmetric waves"
%   (doi: 10.1016/j.coastaleng.2010.02.005)
%
% JH

    if nargin < 5, phi=0; end
    if nargin < 4, deg=0; end

    assert( deg>=-1 && deg<=1, 'Bad degree' );
    
    t = 0 : (1/fs) : len;
    u = 2*pi*omg*t;
    
    fac = sqrt(max( 1 - deg*deg, 0 ));
    q = sin(u) + deg*sin(phi) / (1 + fac);
    r = max(1 - deg*cos(u + phi), eps);
    x = fac * (q./r);
    
    if nargout == 0
        plot( t, x, 'k-', 'LineWidth', 2 );
    end
    
end
