function imf = imf_normalise( imf, thresh, maxiter )
%
% imf = emd.imf_normalise( imf, thresh=1e-6, maxiter=3 )
%
% Normalise amplitude of IMFs in [-1,1].
% Method decribed as part of the AM-FM transform in [1].
%
% References:
%   [1] Huang 2009, "On Instantaneous Frequency" (doi: 10.1142/s1793536909000096)
%
% JH

    if nargin < 3, maxiter=3; end
    if nargin < 2, thresh=1e-6; end
    
    % for each imf
    [n,m] = size(imf);
    for i = 1:m
        
        % divide by the combined amplitude envelope
        it = 0;
        [~,env] = emd.interp_envelope(abs(imf(:,i)));
        while abs(sum(env)-n) > thresh && it < maxiter
            
            it = it + 1;
            imf(:,i) = imf(:,i) ./ env;
            [~,env] = emd.interp_envelope(abs(imf(:,i)));
            
        end
        
    end
    
    % make sure all values are in [-1,1]
    imf = min( max(imf,-1), 1 );
    
end
