function [lo,up] = interp_phase( phase, cycle )
%
% [lo,up] = emd.interp_phase( phase, cycle )
%
%   Smooth the phase to remove within-cycle variations.
%   Second input should be Nx2 with first/last index of good cycles in each row.
%   See emd.cycle_label
%
%
% **NOTE** 
%   THIS FUNCTION DOES NOT ACCEPT PHASE MATRICES
%   The columns of the phase matrix output by emd.imf_analyse should be 
%   processed individually.
%
%
% See also: emd.cycle_label
%
% JH

    assert( isvector(phase), 'This function does not accept phase matrices.' );
    if nargin < 2
        [~,cycle] = emd.cycle_label(phase);
    elseif isstruct(cycle)
        cycle = cycle.index;
    end

    n = numel(phase);
    t = transpose(1:n);
    
    % interpolate phase at 0 and 2*pi
    lo = padded_interp( cycle(:,1), phase, t );
    up = padded_interp( cycle(:,2), phase, t );
    
    % show result for debug
    if nargout == 0
        
        plot( phase, 'k-', 'LineWidth', 1 ); hold on;
        m = size(cycle,1);
        for i = 1:m
            k = cycle(i,1):cycle(i,2);
            plot( k, phase(k), 'k-', 'LineWidth', 2 );
        end
        
        plot( lo, 'b-', 'LineWidth', 1 );
        plot( up, 'r-', 'LineWidth', 1 ); hold off;
        
    end
    
end

function p = padded_interp(k,x,t)
    n = numel(x);
    k = unique([1;k;n]);
    p = interp1( k, x(k), t, 'pchip' );
end