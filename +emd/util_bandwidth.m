function [lo,up] = util_bandwidth( x, fs, prc )
%
% [lo,up] = util_bandwidth( x, fs, prc=[1,99] )
%
%   Estimate frequency bandwidth of input signal using cumulative spectral power.
%   The spectrum is estimated using a 5-fold Welch method with 1/3 overlap.
%   The lower and upper frequencies correspond to percentiles of the cumulative power.
%
% JH

    if nargin < 3, prc=[1,99]; end

    x = x(:);
    n = numel(x);
    
    [p,f] = pwelch( x - mean(x), floor(n/5), floor(n/15), [], fs );
    
    p = cumsum(p);
    t = prctile(p,prc);
    
    klo = find( p >= t(1), 1, 'first' );
    kup = find( p >= t(2), 1, 'first' );
    
    lo = f(klo);
    up = f(kup);
    
    %
    % NOTE TO SELF:
    %
    % Highest frequency can also be estimated with:
    %   fs * emd.nzc(emd.sift(x)) / n;
    %

end