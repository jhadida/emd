function [stat,bins] = cycle_stats( phase, value, varargin )
%
% [hist,bins] = emd.cycle_stats( phase, value, nbins=23 )
% 
%   Compute circular statistics by binning values around the circle.
%   Scalar values are expanded to the same size as the input phase.
%
%
% [hist,bins] = emd.cycle_stats( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   function        DEFAULT: @mean
%                   Function to be applied within each bin.
%   
%   fill            DEFAULT: 0
%                   Value to be used for empty bins.
%
%   mask            DEFAULT: []
%                   Mask to be applied to phase and values before binning.
%
%   good_cycles     DEFAULT: false
%                   Filter phase and values within good cycles only.
%                   Overrides mask option.
%
%
% EXAMPLES
% --------
%
%   To compute the histogram of all values:
%       emd.cycle_stats( phase, values, 'fun', @sum )
%
%   To compute the std of values within each bin:
%       emd.cycle_stats( phase, values, 'fun', @std )
%
%
% JH

    [nbins,fun,fill,good,mask] = parse_inputs(varargin{:});
    
    if good
        n = size(phase,2);
        mask = cell(1,n);
        for i = 1:n
            mask{i} = emd.cycle_label( phase(:,i) );
        end
        mask = horzcat(mask{:}) > 0;
    end
    
    if isscalar(value)
        value = value * ones(size(phase));
    end
    if ~isempty(mask)
        phase = phase(mask);
        value = value(mask);
    end

    bins = (1:nbins)*(2*pi);
    [~,ind] = histc( mod(phase(:),2*pi), bins );
    stat = accumarray( ind, value(:), [nbins,1], fun, fill );

end

function [nbins,fun,fill,good,mask] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.FunctionName = 'emd.cycle_stats';
    
    p.addOptional( 'nbins', 23, @(x) v(x,{'double'},{'scalar','positive','integer'}) );
    p.addParameter( 'function', @mean, @(x) v(x,{'function_handle'},{}) );
    p.addParameter( 'fill', 0, @(x) v(x,{'double'},{'scalar'}) );
    p.addParameter( 'good_cycles', false, @(x) v(x,{'logical'},{'scalar'}) );
    p.addParameter( 'mask', [] );
    
    p.parse(varargin{:});
    r = p.Results;
    
    nbins = r.nbins;
    fun = r.function;
    fill = r.fill;
    good = r.good_cycles;
    mask = logical(r.mask);

end