function out = spectrum( time, ana, varargin )
%
% sp = emd.spectrum( time, ana, ... )
%
%   Compute time-frequency spectrum across IMFs.
%
%   A common frequency range is determined prior to computing the spectra, unless 
%   manually specified via option 'frange'.
%
%   Output is a struct with fields:
%       time
%       freq
%       tf
%
%
% OPTIONS
% -------
%
%   trange      DEFAULT: []
%               Time-range to be used for each spectrum in the batch.
%               If ommitted, we take ceil(sqrt(nt)) as number of bins.
%
%   frange      DEFAULT: []
%               Frequency range to be used for each spectrum in the batch.
%               If ommitted, a common range is computed across IMFs.
%
%   mode        DEFAULT: amplitude
%               Transform the amplitude prior to computing the spectra:
%                   amplitude
%                   magnitude
%                   power       
%                   
%
% See also: emd.joint_histogram, emd.imf_analyse
%
% JH

    [st,sf,mode,other] = parse_inputs( varargin{:} );

    n = numel(time);
    fs = 1/(time(2)-time(1));
    
    % amplitude
    switch mode
        case 'amplitude'
            other.weights = ana.amp;
        case 'magnitude'
            other.weights = ana.amp/sqrt(fs);
        case 'power'
            other.weights = ana.amp.^2/fs;
        otherwise
            error( 'Unknown mode: %s', mode );
    end
    
    % compute spectrum
    if isempty(sf)
        sf = emd.util_frange(ana.frq);
    end
    if isempty(st)
        st = [time(1), time(end), ceil(sqrt(n))];
    elseif isscalar(st)
        st = [time(1), time(end), st];
    end
    
    [out.tf, out.time, out.freq] = emd.joint_hist( time, ana.frq, 'yrange', sf, 'xrange', st, other );
    
    % plot result for debug
    if nargout == 0
        emd.implot( out.time, out.freq, out.tf );
    end
    
end

function [trange,frange,mode,other] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.KeepUnmatched = true;
    p.FunctionName = 'emd.spectrum';
    
    p.addParameter( 'trange', [], @(x) v(x,{'double'},{'nonnegative'}) );
    p.addParameter( 'frange', [], @(x) v(x,{'double'},{'nonnegative'}) );
    p.addParameter( 'mode', 'amplitude', @(x) ismember(x,{'magnitude','amplitude','power'}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    frange = r.frange;
    trange = r.trange;
    mode = r.mode;
    other = p.Unmatched;

end
