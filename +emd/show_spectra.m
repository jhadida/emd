function h = show_spectra( sp, fun )
%
% h = emd.show_spectra( sp, fun=@(x) mean(x,3) )
%
%   Combine multiple input spectra using specified function, and display
%   as a frequency-by-time image.
%
%
% JH

    if nargin < 2, fun = @(x) mean(x,3); end
    
    if isscalar(sp)
        tf = sp.tf;
    else
        tf = fun(cat(3,sp.tf));
    end
    
    sx = sp(1).time;
    sy = sp(1).freq;
    
    h = emd.implot( sx, sy, tf );
    xlabel( 'Time (sec)' ); ylabel( 'Frequency (Hz)' );
    
end