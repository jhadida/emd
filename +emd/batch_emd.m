function imf = batch_emd( time, data, met, varargin )
%
% imf = batch_emd( time, data, method, options )
%
%   Compute EMD for each column in data matrix.
%   Output a cell of IMF matrices.
%
%
% INPUTS
% ------
%
%   method      DEFAULT: regular
%               EMD method used to compute IMFS:
%
%                   regular     @emd.emd
%                   complete    @emd.emd_complete
%                   ensemble    @emd.emd_ensemble
%                   harmonic    @emd.emd_harmonic
%
%   options     DEFAULT: struct()
%               Structure of options to be passed to the EMD method.
%               The sampling frequency is set by default when using
%               the harmonic method.
%
%
% See also: emd.emd, emd.emd_complete, emd.emd_ensemble, emd.emd_harmonic
%
% JH

    if nargin < 3, met='regular'; end

    [nt,ns,fs] = emd.util_chkts(time,data);
    
    % iterate over each column
    imf = cell(1,ns);
    for i = 1:ns
        switch lower(met)
            case {'r','emd','default','regular'}
                imf{i} = emd.emd( data(:,i), varargin{:} );
            case {'c','complete','complete_ensemble'}
                imf{i} = emd.emd_complete( data(:,i), varargin{:} );
            case {'e','ensemble'}
                imf{i} = emd.emd_ensemble( data(:,i), varargin{:} );
            case {'h','harmonic'}
                imf{i} = emd.emd_harmonic( data(:,i), fs, varargin{:} );
            otherwise
                error( 'Unknown EMD method: %s', met );
        end
    end
    
end