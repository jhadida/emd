function h = implot( x, y, z, as_surf )
%
% h = emd.implot( x, y, z, as_surf )
%
%   Display image/surface data.
%
%
% JH

    if nargin < 4, as_surf=false; end

    if as_surf
        h = surf( x, y, z, 'EdgeColor', 'none', 'FaceAlpha', 0.8 );
    else
        if ~isvector(x), x=x(1,:); end
        if ~isvector(y), y=y(:,1); end
        h = imagesc( x, y, z );
        set( gca, 'YDir', 'normal' ); 
    end
    
    colormap( gca, 'jet' ); colorbar; 
    caxis([ 0, emd.prctile(z,99) ]);

end