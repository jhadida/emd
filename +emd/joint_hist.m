function [H,hx,hy] = joint_hist( x, y, varargin )
%
% [H,hx,hy] = emd.joint_hist( x, y )
%
%   Compute joint-histogram of x and y.
%
%
% [H,hx,hy] = emd.joint_hist( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   weights     DEFAULT: 1
%               Associated weights to be accumulated in each bin.
%
%   xrange      DEFAULT: []
%               Binning range for coordinate x, formatted as:
%                   [xmin, xmax, nbins]
%               Default value is computed using emd.range.
%
%   yrange      DEFAULT: []
%               Similarly for coordinate y.
%
%   sparse      DEFAULT: false
%               Return a sparse matrix.
%
%
% See also: emd.range
%
% JH

    [w,xr,yr,sp] = parse_inputs(varargin{:});
    
    % find common dimensions for x and y
    assert( ismatrix(x) && ismatrix(y), 'Input coordinates should be matrices.' );
    cx = size(x,2);
    cy = size(y,2);
    
    if cx < cy, x = repmat( x, 1, cy/cx ); end
    if cy < cx, y = repmat( y, 1, cx/cy ); end
    
    n = numel(x);
    assert( numel(y) == n, 'Size mismatch between input coordinates.' );
    
    % process weights
    if isscalar(w), w=w*ones(n,1); end
    assert( numel(w)==n, 'Bad weights.' );
    
    % vectorise and remove all non-finite elements
    x = x(:);
    y = y(:);
    w = w(:);
    
    m = isfinite(x) & isfinite(y) & isfinite(w);
    x = x(m);
    y = y(m);
    w = w(m);
    
    % bin coordinates
    xr = emd.range( x, xr );
    yr = emd.range( y, yr );
    
    [xe,hx] = emd.range2bins( xr );
    [ye,hy] = emd.range2bins( yr );
    
    [~,xi] = histc(x,xe(2:end));
    [~,yi] = histc(y,ye(2:end));
    
    % create histogram
    m = xi > 0 & yi > 0;
    H = accumarray( [yi(m),xi(m)], w(m), [yr(3),xr(3)], [], [], sp );
    
    % automatically display if no output
    if nargout == 0
        emd.implot( hx, hy, H );
    end
    
end

function [w,xr,yr,sp] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.FunctionName = 'emd.joint_hist';
    
    p.addParameter( 'weights', 1 );
    p.addParameter( 'xrange', [] );
    p.addParameter( 'yrange', [] );
    p.addParameter( 'sparse', false, @(x) v(x,{'logical'},{'scalar'}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    w = r.weights;
    xr = r.xrange;
    yr = r.yrange;
    sp = r.sparse;

end
