function phase = imf_quadrature( imf, donorm )
%
% phase = emd.imf_quadrature( imf, donorm=true )
%
% Return phase quadrature obtained from normalised IMFs.
%
% See also: emd.imf_normalise
%
% JH

    if nargin < 2, donorm=true; end

    if donorm
        imf = emd.imf_normalise(imf);
    end
    
    phase = sqrt( 1 - imf.*imf );
    
    mask = diff(imf,1,1);
    mask = sign(mask);
    mask = [mask; mask(end,:)];
    
    phase = atan2( imf, mask.*phase );
    phase = mod( phase, 2*pi );

end