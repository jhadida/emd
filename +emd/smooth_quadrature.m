function [p,f] = smooth_quadrature( y, fs )
%
% [p,f] = emd.smooth_quadrature( x, fs )
%
%   Smooth analytic features using the quadrature transform.
%
%
% See also: emd.imf_normalise, emd.diff_filter
% 
% JH

    if nargin < 2, fs=1; end
    
    n = size(y,2);
    assert( all(y(:) >= -1 & y(:) <= 1), 'Input should be between -1 and 1.' );
    
    x = sqrt(1 - y.^2);
    mask = diff(y,1,1);
    mask = sign(mask);
    mask = [mask; mask(end,:)];
    x = mask .* x;
    
    f = x .* emd.diff_filter( y, fs ) - y .* emd.diff_filter( x, fs );
    f = max( f, 0 ) / (2*pi);
    p = cumsum([ zeros(1,n); f(1:end-1,:) ],1) * (2*pi/fs);

end