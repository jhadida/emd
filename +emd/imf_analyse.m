function out = imf_analyse( imf, varargin )
%
% ana = emd.imf_analyse( imf, fs=1 )
%
%   Return instantaneous amplitude, phase and frequencies for each IMF, as a struct 
%   with fields:
%       amp     
%       phs
%       frq
%   where each field is the same size as the input IMF matrix.
%
%   Sampling frequency can be set to return frequencies in Hz (instead of per-cycle).
%
%
% ana = emd.imf_analyse( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   method      DEFAULT: huang
%               Method used to compute the instantaneous phase and amplitude:
%
%                   hilbert     The hilbert method takes both phase and amplitude
%                               from the analytic signal of raw IMFs.
%
%                   huang       The huang method takes the Hilbert phase from the 
%                               normalised IMFs, and the amplitude from their upper 
%                               envelope.
%
%                   quadrature  The quadrature transform is a marginally faster 
%                               alternative to the Hilbert transform, using 
%                               normalised IMFs.
%
% 
% See also: emd.imf_normalise, emd.smooth_analytic_impl, emd.smooth_analytic_expl, emd.smooth_quadrature
%
% JH

    [fs,method] = parse_inputs(varargin{:});

    % compute amplitudes
    n = size(imf,2);
    switch lower(method)
        
        case {'hilb','hilbert'}
            [a,p,f] = emd.smooth_analytic_impl(imf,fs);
            
        case {'xhilb','xhilbert'}
            [a,p,f] = emd.smooth_analytic_expl(imf,fs);
            
        case {'nht','huang'}
            a = emd.imf_normalise(imf);
            [~,p,f] = emd.smooth_analytic_impl(a,fs);
            for i = 1:n
                [~,a(:,i)] = emd.interp_envelope(imf(:,i));
            end
            
        case {'quad','quadrature'}
            a = emd.imf_normalise(imf);
            [p,f] = emd.smooth_quadrature(a,fs);
            for i = 1:n
                [~,a(:,i)] = emd.interp_envelope(imf(:,i));
            end
            
        otherwise
            error('Unknown method: %s',method);
        
    end
    
    %shg; histogram(f(:),31); pause(1);
    
    % wrap outputs
    out.amp = a;
    out.phs = p;
    out.frq = f;

end

function [fs,method] = parse_inputs(varargin)

    v = @validateattributes;
    p = inputParser;
    p.FunctionName = 'emd.imf_analyse';
    
    p.addOptional( 'fs', 1, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'method', 'huang', @(x) v(x,{'char'},{}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    fs = r.fs;
    method = r.method;

end
