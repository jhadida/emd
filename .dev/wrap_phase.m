function p = wrap_phase( p, off, ncyc )
%
% p = wrap_phase( p, off, c=1 )
%
% [0, c*2pi)     off=0
% [-c*pi, c*pi)  off=pi
%

    if nargin < 3, ncyc = 1; end
    if nargin < 2, off = 0; end
    
    bnd = ncyc * 2*pi;
    off = ncyc * off;
    
    p = mod( p+off, bnd ) - off;

end