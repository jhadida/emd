function p = quadrature(x)
%
% Direct quandrature of normalised IMF.
%

    p = atan2( x, sqrt(1 - x.*x) );
    
end