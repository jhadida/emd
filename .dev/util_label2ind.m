function ind = util_label2ind( lab, fl )
%
% ind = util_label2ind( label, first_last=false )
%
%   Convert label vector obtained with cycle_label to a set of indices.
%
% JH

    if nargin < 2, fl=false; end
    assert( isvector(lab), 'This function only accepts vector labels.' );

    ind = bwconncomp( lab > 0 );
    ind = ind.PixelIdxList;
    
    if fl
        ind = cellfun( @(k) k([1,end]), ind, 'UniformOutput', false );
        ind = vertcat( ind{:} );
    end

end