function out = imf_analyse( imf, varargin )
%
% ana = emd.imf_analyse( imf, fs=1 )
%
%   Return instantaneous amplitude, phase and frequencies for each IMF, as a struct 
%   with fields:
%       amp     
%       phs
%       frq
%   where each field is the same size as the input IMF matrix.
%
%   Sampling frequency can be set to return frequencies in Hz (instead of per-cycle).
%
%
% ana = emd.imf_analyse( _, Name, Value )
%
%
% OPTIONS
% -------
%
%   method      DEFAULT: hilbert
%               Method used to compute the instantaneous phase and amplitude:
%
%                   hilbert     The hilbert method takes both phase and amplitude
%                               from the analytic signal of raw IMFs.
%
%                   huang       The huang method takes the Hilbert phase from the 
%                               normalised IMFs, and the amplitude from their upper 
%                               envelope.
%
%                   quadrature  The quadrature transform is a marginally faster 
%                               alternative to the Hilbert transform, using 
%                               normalised IMFs.
%
%
%   medfilt     DEFAULT: 0
%               Width of median filter applied to instantaneous phases prior to
%               computing frequencies. 
%
%               This should not be necessary in general; we already use a smooth 
%               differentiation method to estimate frequencies, and recompute the 
%               phase by derivation in order to get smooth estimates. Use only in
%               if additional smoothness is needed.
%               
% 
% See also: emd.imf_normalise, emd.imf_quadrature, emd.diff_filter
%
% JH

    [fs,method,fwidth] = parse_inputs(varargin{:});

    % compute amplitudes
    n = size(imf,2);
    switch lower(method)
        
        case {'hilb','hilbert'}
            p = hilbert(imf);
            a = abs(p);
            p = angle(p);
            
        case {'nht','huang'}
            a = emd.imf_normalise(imf);
            p = angle(hilbert(a));
            
            for i = 1:n
                [~,a(:,i)] = emd.interp_envelope(imf(:,i));
            end
            
        case {'quad','quadrature'}
            a = emd.imf_normalise(imf);
            p = emd.imf_quadrature(a,false);
            
            for i = 1:n
                [~,a(:,i)] = emd.interp_envelope(imf(:,i));
            end
            
        otherwise
            error('Unknown method: %s',method);
        
    end
    
    % compute smooth phase and frequency
    p = unwrap(p, [], 1);
    if fwidth > 1
        p = medfilt1(p,fwidth);
    end
    %shg; subplot(2,1,1); plot(p); 
    
    f = emd.diff_filter( p, fs ) / (2*pi);
    p = cumsum([ zeros(1,n); f(1:end-1,:) ],1) * (2*pi/fs);
    %f(f < 0) = nan; % set negative frequencies to NaN
    
    %subplot(2,1,2); plot(p); xlabel('Time'); pause(1);
    
    % wrap outputs
    out.amp = a;
    out.phs = p;
    out.frq = f;

end

function [fs,method,fwidth] = parse_inputs(varargin)

    v = @validateattributes;
    vs = @validatestring;
    p = inputParser;
    p.FunctionName = 'emd.imf_analyse';
    
    p.addOptional( 'fs', 1, @(x) v(x,{'double'},{'scalar','positive'}) );
    p.addParameter( 'method', 'hilbert', @(x) v(x,{'char'},{}) );
    p.addParameter( 'medfilt', 0, @(x) v(x,{},{'scalar','integer'}) );
    
    p.parse(varargin{:});
    r = p.Results;
    
    fs = r.fs;
    method = r.method;
    fwidth = r.medfilt;

end
