
# Empirical Mode Decomposition Toolbox

This is a standalone Matlab toolbox to compute _Empirical Mode Decompositions_ (EMD) on time-series data. If you have never heard of this, it is basically a robust method to compute instantaneous frequency features.

<div align="center"><img src="./tfspectrum.png" alt="Time-frequency-spectrum" width="70%" /></div>

This toolbox was adapted from [**Andrew Quinn**](https://github.com/AJQuinn)'s Python module (to be released soon), and is intended for research purposes only. It contains all the basics, and has been reasonably well tested; but please refer to Andrew's Python module for fancy waveform analysis, beautiful plots, words of wisdom, and more 🌈 🦄

## Quick install

First, choose where to store this code (a good default choice is `userpath()` in Matlab).

To clone and add this package to your path, type from the Matlab console:
```matlab
target = fullfile( userpath(), 'emd' );
setenv('EMD_TARGET',target);
!git clone https://gitlab.com/jhadida/emd.git "$EMD_TARGET"
addpath(target);
```

## Example

For a quick demo, type:
```
emd.demo
```

This should show the above time-frequency spectrum in a new figure. 

If you open this script, you will see that the main function called is `emd.run`, which computes the EMD for all channels of the input time-series matrix, analyses the corresponding IMFs, and gathers time-frequency spectra.

## Usage

More generally, the functions you will most likely be interested in are:
```
emd.batch_emd           compute IMFs on a TxN time-series matrix
emd.batch_analyse       compute corresponding instantaneous amplitude
                         and frequency given a cell of IMFs
emd.batch_spectra       compute time-frequency spectra from a given
                         set of analysis results
```

Check the helptext of these functions for more information. Briefly about the outputs:

 - `emd.batch_emd` returns IMFs as a cell-array of TxM matrices with variable M, where each cell corresponds to a channel (i.e. column) in the time-series matrix, and M corresponds to the number of IMFs found for that channel;
 - `emd.batch_analyse` returns a struct-array (one for each channel) with fields `amp, phs, frq`, where each field in struct `k` is a matrix of same size as the IMF matrix for channel `k`, respectively with the instantaneous amplitude, phase and frequencies for each IMF;
 - `emd.batch_spectra` also returns a struct-array (one for each channel) with fields `tf, time, freq`, where `tf` is the FxT time-frequency matrix (real-valued) computed across IMFs for a given channel, and `time, freq` are vectors with the corresponding time and frequency bins.

These batch scripts mainly call the following:

 - the functions `emd.emd_*` actually compute the IMFs for a single-channel time-course, and rely on the sifting functions implemented in `emd.sift_*`;
 - the function `emd.imf_analyse` is the main entry-point to compute instantaneous features from a set of IMFs, and relies mainly on the implementations of functions `emd.imf_*` and `emd.smooth_*` for smooth analytic transforms.

There are other goodies (currently undocumented and poorly tested, sorry 😥) which you might want to explore through the helptext, such as the `emd.cycle_` functions and `emd.waveform`. Feel free to [open an issue](https://gitlab.com/jhadida/emd/issues) if you find something fishy.
